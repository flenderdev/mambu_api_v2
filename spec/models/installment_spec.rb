# frozen_string_literal: true

require 'spec_helper'
require 'json'

describe ::MambuApiV2::Installment, type: :model do
  let(:installment_attributes) { JSON.parse(File.open('spec/fixtures/json/installment.json').read, symbolize_names: true) }
  let(:installment) { described_class.new.build_from_hash(installment_attributes) }

  describe '#fetch_amount' do
    it 'should fetch sum of paid amounts' do
      expect(installment.fetch_amount(:paid)).to eql 1500.0
    end

    it 'should fetch sum of due amounts' do
      expect(installment.fetch_amount(:due)).to eql 1500.0
    end

    it 'should raise error with proper name' do
      allow_any_instance_of(described_class).to receive(:principal).and_return(nil)
      expect { installment.fetch_amount(:paid) }.to raise_error('Error during installment parsing')
    end
  end
end
