require 'spec_helper'

describe ::MambuApiV2::Installments::GetInstallmentService do
  let(:due_to) { Date.today.strftime('%Y-%m-%d') }
  let(:due_from) { Date.today.strftime('%Y-%m-%d') }
  let(:valid_call) { described_class.call(due_to, due_from) }

  before(:each) { allow_any_instance_of(::MambuApiV2::InstallmentsApi).to receive(:get_all).and_return([]) }

  it 'should verify the return type' do
    expect(valid_call).to be_an_instance_of(Array)
  end

  it 'should verify arguments' do
    instance = described_class.new(due_to, due_from)
    expect(instance.due_to).to eql(due_to)
    expect(instance.due_from).to eql(due_from)
  end

  it 'should raise error if arguments are not passed' do
    expect { described_class.call('test') }.to raise_error(ArgumentError)
  end

  it 'should verify payload' do
    instance = described_class.new(due_to, due_from)
    expect(instance.send(:payload, 100, 0)).to be_an_instance_of(Hash)
  end
end