require 'spec_helper'

describe MambuApiV2::Installments::Client do
  it '#get_installments should call get_installments to retrieve all Installments' do
    expect(::MambuApiV2::Installments::GetInstallmentService).to receive(:call).with(Date.today.to_s, Date.today.to_s)
    described_class.get_installments(Date.today.to_s, Date.today.to_s)
  end

  context 'verifies number of arguments' do
    subject { described_class }

    it { should respond_to(:get_installments).with(2).argument }
  end
end
