require 'spec_helper'

describe MambuApiV2::FundingSources::Client do
  it '#sell should call sell service' do
    expect(::MambuApiV2::FundingSources::SellService).to receive(:call).with('test', [{test: 'test'}])
    described_class.sell('test', [{test: 'test'}])
  end
end