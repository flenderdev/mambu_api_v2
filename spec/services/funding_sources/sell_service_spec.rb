require 'spec_helper'

describe MambuApiV2::FundingSources::SellService do
  let(:purchase_hash) { {deposit_account_key: 'example', amount: 15_000, price: 16_000} }
  let(:valid_call) { described_class.call('test', purchase_hash) }
  let(:purchase) { ::MambuApiV2::FundingSourcePurchase.new(purchase_hash.merge(depositAccountKey: 'example')) }
  let(:valid_call_2) { described_class.call('test', purchase) }

  before(:each) { allow_any_instance_of(::MambuApiV2::FundingSourcesApi).to receive(:sell).and_return(true) }

  it 'should normalize purchases' do
    instance = described_class.new('test', 1)
    expect(instance.purchases).to eql [1]
  end

  it 'should accept json style keys' do
    purchase_hash[:depositAccountKey] = purchase_hash.delete(:deposit_account_key)
    expect { described_class.call('test', purchase_hash) }.not_to raise_error
  end

  it 'should check purchases type' do
    expect { described_class.call('test', [1]) }.to raise_error(MambuApiV2::ServiceError)
    expect { valid_call }.not_to raise_error
    expect { valid_call_2 }.not_to raise_error
  end

  it 'should validate purchase' do
    invalid_hashes = [{deposit_account_key: 'example'}, {amount: 15_000}, {price: 16_000},
                      {deposit_account_key: 'example', amount: 15_000}, {deposit_account_key: 'example', price: 15_000}]
    invalid_hashes.each do |invalid_hash|
      expect { described_class.call('test', invalid_hash) }.to raise_error(MambuApiV2::ServiceError)
      expect {
        described_class.call('test', ::MambuApiV2::FundingSourcePurchase.new(invalid_hashes))
      }.to raise_error(MambuApiV2::ServiceError)
    end
    expect { valid_call }.not_to raise_error
    expect { valid_call_2 }.not_to raise_error
  end

  it 'should make api call' do
    parsed_purchase_hash = {amount: 15000, depositAccountKey: 'example', price: 16000}
    expect(::MambuApiV2::FundingSourcesApi).to receive(:new).twice.and_call_original
    expect(::MambuApiV2::SellFundingSourceAction).to receive(:new).twice.with(purchases: [parsed_purchase_hash])
    valid_call
    valid_call_2
  end
end