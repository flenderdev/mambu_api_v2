require 'spec_helper'

describe MambuApiV2::LoanAccounts::Client do
  let!(:payload) { { 'filterCriteria': filters, 'sortingCriteria': sorting_details } }
  let!(:filters) do
    [
      {
        'field': 'accountState',
        'operator': 'IN',
        'values': %w[ACTIVE_IN_ARREARS ACTIVE]
      },
      {
        'field': 'daysLate',
        'operator': 'BETWEEN',
        'value': '10',
        'secondValue': '100'
      },
      {
        'field': 'encodedKey',
        'operator': 'IN',
        'values': ['8a85868677af7b0d0177afeb91a803e6']
      }
    ]
  end
  let!(:sorting_details) { { 'field': 'encodedKey', 'order': 'ASC' } }
  let!(:http_options) { { details_level: 'FULL' } }

  it '#search_with_http_info should call service' do
    expect(::MambuApiV2::LoanAccounts::SearchWithHttpInfoService).to receive(:call)
    described_class.search_with_http_info(payload, http_options)
  end
end
