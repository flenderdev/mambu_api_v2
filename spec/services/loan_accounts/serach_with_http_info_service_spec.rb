require 'spec_helper'

describe MambuApiV2::LoanAccounts::SearchWithHttpInfoService do
  let!(:payload) { { 'filterCriteria': filters, 'sortingCriteria': sorting_details } }
  let!(:filters) do
    [
      {
        'field': 'accountState',
        'operator': 'IN',
        'values': %w[ACTIVE_IN_ARREARS ACTIVE]
      },
      {
        'field': 'daysLate',
        'operator': 'BETWEEN',
        'value': '10',
        'secondValue': '100'
      },
      {
        'field': 'encodedKey',
        'operator': 'IN',
        'values': ['8a85868677af7b0d0177afeb91a803e6']
      }
    ]
  end
  let!(:sorting_details) { { 'field': 'encodedKey', 'order': 'ASC' } }
  let!(:http_options) { { details_level: 'FULL' } }

  before(:each) { allow_any_instance_of(::MambuApiV2::LoanAccountsSearchApi).to receive(:search_with_http_info).and_return([true]) }

  it 'should make api call' do
    expect(::MambuApiV2::LoanAccountsSearchApi).to receive(:new).and_call_original
    expect(::MambuApiV2::LoanAccountSearchCriteria).to receive(:new).and_call_original
    described_class.call(payload, http_options)
  end
end
