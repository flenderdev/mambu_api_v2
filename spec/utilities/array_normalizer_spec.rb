require 'spec_helper'

describe MambuApiV2::ArrayNormalizer do
  context '#normalize' do
    it 'should return nil if nil provided' do
      expect(described_class.normalize(nil)).to eql nil
    end

    it 'should array if array provided' do
      expect(described_class.normalize([1])).to eql [1]
    end

    it 'should convert to array if regular value provided' do
      expect(described_class.normalize(1)).to eql [1]
    end
  end
end
