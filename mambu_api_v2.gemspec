# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "mambu_api_v2/version"

Gem::Specification.new do |spec|
  spec.name          = "mambu_api_v2"
  spec.version       = MambuApiV2::VERSION
  spec.platform    = Gem::Platform::RUBY
  spec.authors       = ["Bazeltsev Kirill"]
  spec.email         = ["kirill.bazeltsev@gmail.com"]

  spec.summary       = 'A Ruby library for Flender App'
  spec.description   = 'A Ruby library for Flender App'
  spec.homepage      = "https://bitbucket.org/flenderdev/mambu_api_v2"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "gem.flender.ie"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.add_runtime_dependency 'typhoeus', '~> 1.0', '>= 1.0.1'
  spec.add_runtime_dependency 'json', '~> 2.1', '>= 2.1.0'
  spec.add_runtime_dependency 'addressable', '~> 2.3', '>= 2.3.0'

  spec.add_development_dependency 'bundler', '~> 1.17.2'
  spec.add_development_dependency 'byebug'
  spec.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'
  spec.add_development_dependency 'vcr', '~> 3.0', '>= 3.0.1'
  spec.add_development_dependency 'webmock', '~> 1.24', '>= 1.24.3'
  spec.add_development_dependency 'autotest', '~> 4.4', '>= 4.4.6'
  spec.add_development_dependency 'autotest-rails-pure', '~> 4.1', '>= 4.1.2'
  spec.add_development_dependency 'autotest-growl', '~> 0.2', '>= 0.2.16'
  spec.add_development_dependency 'autotest-fsevent', '~> 0.2', '>= 0.2.12'

  spec.files         = `find *`.split("\n").uniq.sort.select { |f| !f.empty? }
  spec.test_files    = `find spec/*`.split("\n")
  spec.executables   = []
  spec.require_paths = ["lib"]
end