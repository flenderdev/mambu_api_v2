# MambuApiV2::DepositTerms

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_settings** | [**DepositTransactionInterestSettings**](DepositTransactionInterestSettings.md) | Groups all fields having interest purpose concerning a transaction | [optional] 
**overdraft_settings** | [**DepositOverdraftSettings**](DepositOverdraftSettings.md) | Holds the deposit interest settings | [optional] 
**overdraft_interest_settings** | [**DepositOverdraftInterestSettings**](DepositOverdraftInterestSettings.md) | Holds the deposit overdraft interest settings | [optional] 


