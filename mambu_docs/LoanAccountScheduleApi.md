# MambuApiV2::LoanAccountScheduleApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_preview_loan_account_schedule**](LoanAccountScheduleApi.md#get_preview_loan_account_schedule) | **POST** /loans:previewSchedule | Allows retrieval of a loan account schedule for non existing loan account
[**get_schedule_for_loan_account**](LoanAccountScheduleApi.md#get_schedule_for_loan_account) | **GET** /loans/{loanAccountId}/schedule | Allows retrieval of a loan account schedule by provided loan account id or encodedKey.


# **get_preview_loan_account_schedule**
> LoanAccountSchedule get_preview_loan_account_schedule(body)

Allows retrieval of a loan account schedule for non existing loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountScheduleApi.new

body = MambuApiV2::PreviewLoanAccountSchedule.new # PreviewLoanAccountSchedule | Loan account parameters for a schedule to be previewed


begin
  #Allows retrieval of a loan account schedule for non existing loan account
  result = api_instance.get_preview_loan_account_schedule(body)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountScheduleApi->get_preview_loan_account_schedule: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PreviewLoanAccountSchedule**](PreviewLoanAccountSchedule.md)| Loan account parameters for a schedule to be previewed | 

### Return type

[**LoanAccountSchedule**](LoanAccountSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **get_schedule_for_loan_account**
> LoanAccountSchedule get_schedule_for_loan_account(loan_account_id)

Allows retrieval of a loan account schedule by provided loan account id or encodedKey.



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountScheduleApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account


begin
  #Allows retrieval of a loan account schedule by provided loan account id or encodedKey.
  result = api_instance.get_schedule_for_loan_account(loan_account_id)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountScheduleApi->get_schedule_for_loan_account: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 

### Return type

[**LoanAccountSchedule**](LoanAccountSchedule.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



