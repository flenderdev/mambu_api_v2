# MambuApiV2::PreviewLoanAccountSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_settings** | [**InterestSettingsForSchedulePreview**](InterestSettingsForSchedulePreview.md) | The interest settings details for schedule preview. | [optional] 
**disbursement_details** | [**DisbursementDetailsForSchedulePreview**](DisbursementDetailsForSchedulePreview.md) | Details regarding the disbursement | [optional] 
**schedule_settings** | [**ScheduleSettingsForSchedulePreview**](ScheduleSettingsForSchedulePreview.md) | The schedule settings details for schedule preview. | [optional] 
**interest_commission** | **Float** | The value of the interest booked by the organization from the accounts funded by investors. Null if the funds are not enable | [optional] 
**product_type_key** | **String** | The key to the type of product that this account is based on. | 
**loan_amount** | **Float** | The loan amount | 


