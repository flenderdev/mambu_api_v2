# MambuApiV2::RescheduleWriteOffAmounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**principal** | **Float** | Principal write-off amount | [optional] 
**interest** | **Float** | Interest write-off amount | [optional] 
**penalty** | **Float** | Penalty write-off amount | [optional] 
**fee** | **Float** | Fee write-off amount | [optional] 


