# MambuApiV2::ChangePeriodicPaymentLoanAccountInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**periodic_payment** | **Float** | The new periodic payment to be available on the account | 
**value_date** | **DateTime** | The date when to change the periodic payment (as Organization Time) | 
**notes** | **String** | The notes for the change periodic payment action performed on the loan account | [optional] 


