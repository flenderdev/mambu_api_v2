# MambuApiV2::PaymentIdentification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instruction_identification** | **String** | Identifier of a payment instruction | [optional] 
**end_to_end_identification** | **String** | Identifier assigned by the initiating party to the transaction | [optional] 
**transaction_identification** | **String** | Identifier unique for a period assigned by the first initiating party to the transaction | [optional] 


