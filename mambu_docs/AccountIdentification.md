# MambuApiV2::AccountIdentification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**other** | [**OtherAccountIdentification**](OtherAccountIdentification.md) | Other means of identification for the account | [optional] 
**iban** | **String** | The account unique identifier | [optional] 


