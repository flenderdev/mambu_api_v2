# MambuApiV2::RefinanceDisbursementDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_repayment_date** | **DateTime** | The date of the expected first payment | 
**expected_disbursement_date** | **DateTime** | The date of the expected disbursement | [optional] 


