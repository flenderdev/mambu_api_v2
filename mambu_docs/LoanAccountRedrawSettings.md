# MambuApiV2::LoanAccountRedrawSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**restrict_next_due_withdrawal** | **BOOLEAN** | Indicates whether withdrawing amounts that reduce the next due instalment repayment is restricted or not | 


