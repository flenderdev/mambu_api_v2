# MambuApiV2::LoanAccountPayOffInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_details** | [**TransactionDetails**](TransactionDetails.md) | Details for the transactions to be logged for Pay Off action | [optional] 
**external_id** | **String** | External id for Repayment transaction | [optional] 
**notes** | **String** | Extra notes for Repayment transaction logged for Pay Off action | [optional] 
**pay_off_adjustable_amounts** | [**PayOffAdjustableAmounts**](PayOffAdjustableAmounts.md) | Adjustable amounts to be paid | [optional] 


