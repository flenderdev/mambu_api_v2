# MambuApiV2::InterestSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_rate_review_unit** | **String** | The interest rate review unit. Defines the interest rate update frequency measurement unit. | [optional] 
**interest_rate** | **Float** | The interest rate. Represents the interest rate for the loan account. The interest on loans is accrued on a daily basis, which allows charging the clients only for the days they actually used the loan amount. | [optional] 
**interest_rate_source** | **String** | The interest rate source. Represents the interest calculation method: fixed or (interest spread + active organization index interest rate) | [optional] 
**interest_application_method** | **String** | The interest application method. Represents the interest application method that determines whether the interest gets applied on the account&#39;s disbursement or on each repayment. | [optional] 
**interest_charge_frequency** | **String** | The interest change frequency. Holds the possible values for how often is interest charged on loan or deposit accounts | [optional] 
**interest_type** | **String** | The possible values for how we compute and apply the interest | [optional] 
**accrue_interest_after_maturity** | **BOOLEAN** | The accrue interest after maturity. If the product support this option, specify if the interest should be accrued after the account maturity date. | [optional] 
**interest_balance_calculation_method** | **String** | The interest balance calculation method. Represents the option which determines the way the balance for the account&#39;s interest is computed. | [optional] 
**interest_spread** | **Float** | Interest to be added to active organization index interest rate in order to find out actual interest rate | [optional] 
**interest_calculation_method** | **String** | The interest calculation method. Holds the type of interest calculation method. | [optional] 
**interest_rate_review_count** | **Integer** | Interest rate update frequency unit count. | [optional] 
**accrue_late_interest** | **BOOLEAN** | Indicates whether late interest is accrued for this loan account | [optional] 


