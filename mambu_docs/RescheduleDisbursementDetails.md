# MambuApiV2::RescheduleDisbursementDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_repayment_date** | **DateTime** | The date of the expected first payment | 


