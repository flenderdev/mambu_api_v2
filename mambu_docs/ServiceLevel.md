# MambuApiV2::ServiceLevel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | The code for a pre-agreed service or level of service between the parties | [optional] 


