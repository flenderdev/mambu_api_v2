# MambuApiV2::InstallmentAllocationElementAmount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**Amount**](Amount.md) | The installment allocation element amount. | [optional] 


