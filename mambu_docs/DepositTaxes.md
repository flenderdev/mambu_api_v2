# MambuApiV2::DepositTaxes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax_rate** | **Float** | The tax rate that was set or changed in this transaction | [optional] 


