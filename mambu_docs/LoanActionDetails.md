# MambuApiV2::LoanActionDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notes** | **String** | The notes for the action performed on the loan account | [optional] 


