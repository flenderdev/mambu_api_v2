# MambuApiV2::TrancheDisbursementDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expected_disbursement_date** | **DateTime** | The date when this tranche is supposed to be disbursed (as Organization Time) | [optional] 
**disbursement_transaction_key** | **String** | The key of the disbursement transaction logged when this tranche was disbursed. This field will be null until the tranche disbursement | [optional] 


