# MambuApiV2::AccountDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **String** | The currency of the account | [optional] 
**identification** | [**AccountIdentification**](AccountIdentification.md) | The account identification details for the transaction | [optional] 


