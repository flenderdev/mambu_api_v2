# MambuApiV2::RefinanceLoanAccountAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loan_account** | [**RefinanceLoanAccount**](RefinanceLoanAccount.md) | The refinanced loan account details | 
**write_off_amounts** | [**RefinanceWriteOffAmounts**](RefinanceWriteOffAmounts.md) | The refinance action write-off amounts | [optional] 
**top_up_amount** | **Float** | The top-up amount | 


