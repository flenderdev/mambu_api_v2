# MambuApiV2::Installment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**principal** | [**InstallmentAllocationElementAmount**](InstallmentAllocationElementAmount.md) | The original loan amount that is disbursed, excluding interest, fees and penalties. | [optional] 
**number** | **String** | The order number of an installment among all the installments generated for a loan. Loan installments are put in ascending order by due date. | [optional] 
**last_paid_date** | **DateTime** | The installment last paid date. | [optional] 
**parent_account_key** | **String** | The parent account key of the installment | [optional] 
**interest** | [**InstallmentAllocationElementTaxableAmount**](InstallmentAllocationElementTaxableAmount.md) | The interest amount that is charged for the use of the loan. | [optional] 
**penalty** | [**InstallmentAllocationElementTaxableAmount**](InstallmentAllocationElementTaxableAmount.md) | The penalty amount that can be charged for the loan account. | [optional] 
**due_date** | **DateTime** | The installment due date. | [optional] 
**fee** | [**InstallmentAllocationElementTaxableAmount**](InstallmentAllocationElementTaxableAmount.md) | The fee amount that may be applied to the loan account. | [optional] 
**encoded_key** | **String** | The encoded key of the installment, auto generated, unique. | [optional] 
**state** | **String** | The installment state. | [optional] 
**repaid_date** | **DateTime** | The installment repaid date. | [optional] 


