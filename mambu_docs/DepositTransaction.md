# MambuApiV2::DepositTransaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**migration_event_key** | **String** | The migration event encoded key associated with this deposit account. If this account was imported, track which &#39;migration event&#39; they came from | [optional] 
**transaction_details** | [**TransactionDetails**](TransactionDetails.md) | Holds the transaction details | [optional] 
**fees** | [**Array&lt;Fee&gt;**](Fee.md) | All the amounts that have been applied or paid within this transaction and involved predefined fees | [optional] 
**notes** | **String** | Extra notes about this deposit transaction | [optional] 
**affected_amounts** | [**DepositAffectedAmounts**](DepositAffectedAmounts.md) | The amounts affected after completing the transaction | [optional] 
**card_transaction** | [**CardTransaction**](CardTransaction.md) | The card transaction details that correspond to this deposit transaction (if applicable) | [optional] 
**taxes** | [**DepositTaxes**](DepositTaxes.md) | The taxes applied on this transaction | [optional] 
**till_key** | **String** | The till key associated with this transaction | [optional] 
**adjustment_transaction_key** | **String** | The key of the deposit transaction where the adjustment for this transaction was made (if any adjustment was involved) | [optional] 
**type** | **String** | The type of the deposit transaction | [optional] 
**branch_key** | **String** | The branch where the transaction was performed | [optional] 
**terms** | [**DepositTerms**](DepositTerms.md) | The terms of the deposit transaction | [optional] 
**transfer_details** | [**TransferDetails**](TransferDetails.md) | Holds the transfer details | [optional] 
**payment_order_id** | **String** | The payment order id of the deposit transaction, customizable | [optional] 
**encoded_key** | **String** | The encoded key of the deposit transaction, auto generated, unique | [optional] 
**id** | **String** | The id of the deposit transaction, auto generated, unique | [optional] 
**payment_details** | [**PaymentDetails**](PaymentDetails.md) | Holds the payment details | [optional] 
**original_transaction_key** | **String** | The encodedKey of the transaction that was adjusted as part of this one. Available only for adjustment transactions | [optional] 
**amount** | **Float** | How much was added/removed in account | [optional] 
**centre_key** | **String** | The center where the transaction was performed | [optional] 
**external_id** | **String** | The external id of the deposit transaction, customizable, unique | [optional] 
**value_date** | **DateTime** | Date of the entry (eg date of repayment or disbursal, etc.) (as Organization Time) | [optional] 
**creation_date** | **DateTime** | The date when this deposit transaction was created | [optional] 
**user_key** | **String** | The person that performed the transaction | [optional] 
**block_id** | **String** | The block fund id associated with the transaction | [optional] 
**parent_account_key** | **String** | The key of the parent deposit account | [optional] 
**account_balances** | [**DepositTransactionBalances**](DepositTransactionBalances.md) | The account balances changed within the transaction | [optional] 
**booking_date** | **DateTime** | The date when corresponding JE is booked (as Organization Time) | [optional] 
**currency_code** | **String** | The currency in which this transaction was posted | [optional] 


