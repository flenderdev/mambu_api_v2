# MambuApiV2::PeriodicPayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to_installment** | **Integer** | The installment&#39;s position up to which the PMT will be used | 
**encoded_key** | **String** | The encoded key of the periodic payment, auto generated, unique. | [optional] 
**amount** | **Float** | The PMT value used in periodic payment | 


