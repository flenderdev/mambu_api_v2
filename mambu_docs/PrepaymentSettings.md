# MambuApiV2::PrepaymentSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**elements_recalculation_method** | **String** | The elements recalculation method, indicates how the declining balance with equal installments repayments are recalculated. | [optional] 
**principal_paid_installment_status** | **String** | Installment status for the case when principal is paid off (copied from loan product). | [optional] 
**prepayment_recalculation_method** | **String** | Prepayment recalculation method copied from the loan product on which this account is based. | [optional] 
**apply_interest_on_prepayment_method** | **String** | Apply interest on prepayment method copied from loan product on which this account is based. | [optional] 


