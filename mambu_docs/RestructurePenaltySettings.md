# MambuApiV2::RestructurePenaltySettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**penalty_rate** | **Float** | The penalty rate | [optional] 


