# MambuApiV2::LoanAccountFundingApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_funding_sources**](LoanAccountFundingApi.md#delete_funding_sources) | **DELETE** /loans/{loanAccountId}/funding | Deletes all loan account funding sources.
[**delete_single_funding_source**](LoanAccountFundingApi.md#delete_single_funding_source) | **DELETE** /loans/{loanAccountId}/funding/{fundEncodedKey} | Allows to delete loan account single funding source by provided loan account id or encoded key and fund encoded key.


# **delete_funding_sources**
> delete_funding_sources(loan_account_id)

Deletes all loan account funding sources.



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountFundingApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account


begin
  #Deletes all loan account funding sources.
  api_instance.delete_funding_sources(loan_account_id)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountFundingApi->delete_funding_sources: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **delete_single_funding_source**
> delete_single_funding_source(loan_account_id, fund_encoded_key)

Allows to delete loan account single funding source by provided loan account id or encoded key and fund encoded key.



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountFundingApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

fund_encoded_key = 'fund_encoded_key_example' # String | The encoded key of the funding source.


begin
  #Allows to delete loan account single funding source by provided loan account id or encoded key and fund encoded key.
  api_instance.delete_single_funding_source(loan_account_id, fund_encoded_key)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountFundingApi->delete_single_funding_source: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **fund_encoded_key** | **String**| The encoded key of the funding source. | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



