# MambuApiV2::RestructureAccountArrearsSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tolerance_percentage_of_outstanding_principal** | **Float** | The arrears tolerance amount | [optional] 
**tolerance_period** | **Integer** | The arrears tolerance period value | [optional] 


