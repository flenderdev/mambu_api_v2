# MambuApiV2::LoanAccountSortingCriteria

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **String** | Contains the field that can be used as sorting selection. Can be native (one from the provided list) or otherwise can specify a custom field using the format [customFieldSetId].[customFieldId]. | 
**order** | **String** | The sorting order: ASC or DESC. The default order is DESC. | [optional] 


