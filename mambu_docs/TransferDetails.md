# MambuApiV2::TransferDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linked_loan_transaction_key** | **String** | The key of the related loan transaction | [optional] 
**linked_deposit_transaction_key** | **String** | The key of the related deposit transaction | [optional] 


