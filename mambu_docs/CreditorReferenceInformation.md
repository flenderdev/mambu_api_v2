# MambuApiV2::CreditorReferenceInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference** | **String** | The reference information of the creditor&#39;s underlying documents | [optional] 
**reference_type** | **String** | The type of creditor reference | [optional] 
**reference_issuer** | **String** | The entity that assigns the reference type | [optional] 


