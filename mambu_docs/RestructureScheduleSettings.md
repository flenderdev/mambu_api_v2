# MambuApiV2::RestructureScheduleSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grace_period** | **Integer** | The grace period | [optional] 
**repayment_period_unit** | **String** | The period of time, within which the payments frequency is set | [optional] 
**periodic_payment** | **Float** | The periodic payment | [optional] 
**fixed_days_of_month** | **Array&lt;Integer&gt;** | The days of the month, when the repayment due dates should be | [optional] 
**repayment_installments** | **Integer** | The number of installments | [optional] 
**repayment_period_count** | **Integer** | The payments frequency per set period of time | [optional] 


