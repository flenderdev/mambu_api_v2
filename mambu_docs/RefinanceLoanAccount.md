# MambuApiV2::RefinanceLoanAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disbursement_details** | [**RefinanceDisbursementDetails**](RefinanceDisbursementDetails.md) | The disbursement details | 
**notes** | **String** | The loan account notes | [optional] 
**principal_payment_settings** | [**RestructurePrincipalPaymentAccountSettings**](RestructurePrincipalPaymentAccountSettings.md) | The principal payment settings | [optional] 
**loan_name** | **String** | The name of the new loan account. | [optional] 
**interest_settings** | [**RestructureInterestSettings**](RestructureInterestSettings.md) | The interest settings | [optional] 
**schedule_settings** | [**RestructureScheduleSettings**](RestructureScheduleSettings.md) | The schedule settings | [optional] 
**encoded_key** | **String** | The encoded key of the entity, generated, globally unique | [optional] 
**product_type_key** | **String** | The key of the loan product that this account is based on | 
**id** | **String** | Id of the new loan account. | [optional] 
**guarantors** | [**Array&lt;Guarantor&gt;**](Guarantor.md) | The guarantees associated with the refinanced loan account | [optional] 
**account_arrears_settings** | [**RestructureAccountArrearsSettings**](RestructureAccountArrearsSettings.md) | The arrears settings | [optional] 
**penalty_settings** | [**RestructurePenaltySettings**](RestructurePenaltySettings.md) | The penalty settings | [optional] 


