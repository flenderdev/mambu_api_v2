# MambuApiV2::ChangeArrearsSettingsInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**arrears_tolerance_period** | **Integer** | The new arrears tolerance period to be available on the account | 
**notes** | **String** | The notes for the change arrears settings action performed on the loan account | [optional] 
**entry_date** | **DateTime** | The date when to change the arrears settings | 


