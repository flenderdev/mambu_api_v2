# MambuApiV2::PaymentTypeInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_level** | [**ServiceLevel**](ServiceLevel.md) | Agreement under which rules the transaction should be processed | [optional] 


