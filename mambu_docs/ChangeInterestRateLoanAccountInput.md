# MambuApiV2::ChangeInterestRateLoanAccountInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_rate** | **Float** | The new interest rate to be available on the account | [optional] 
**interest_spread** | **Float** | The new interest spread to be available on the account | [optional] 
**value_date** | **DateTime** | The date when to change the interest rate (as Organization Time) | 
**notes** | **String** | The notes for the change interest rate action performed on the loan account | [optional] 


