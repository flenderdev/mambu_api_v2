# MambuApiV2::Amount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paid** | **Float** | The paid amount. | [optional] 
**due** | **Float** | The due amount. | [optional] 
**expected** | **Float** | The expected amount, which is sum of paid and due amounts. | [optional] 


