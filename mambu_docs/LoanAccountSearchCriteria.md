# MambuApiV2::LoanAccountSearchCriteria

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sorting_criteria** | [**LoanAccountSortingCriteria**](LoanAccountSortingCriteria.md) | The sorting criteria | [optional] 
**filter_criteria** | [**Array&lt;LoanAccountFilterCriteria&gt;**](LoanAccountFilterCriteria.md) | The list of filtering criteria | [optional] 


