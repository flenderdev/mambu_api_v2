# MambuApiV2::ChangeDueDatesSettingsInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notes** | **String** | The notes for the change due dates settings action performed on the loan account | [optional] 
**entry_date** | **DateTime** | The date when to change the due dates settings | 
**fixed_days_of_month** | **Array&lt;Integer&gt;** | The new fixed days of month to be used on the account | 


