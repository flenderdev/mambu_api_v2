# MambuApiV2::Fee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the predefined fee | [optional] 
**amount** | **Float** | The amount of the fee that was applied/paid in the transaction for the given predefined fee. | [optional] 
**trigger** | **String** | Shows the event that will trigger a fee | [optional] 
**tax_amount** | **Float** | The amount of the taxes on fee that was applied/paid in the transaction. | [optional] 
**predefined_fee_key** | **String** | The encoded key of the predefined fee, auto generated, unique | 


