# MambuApiV2::SellFundingSourceAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchases** | [**Array&lt;FundingSourcePurchase&gt;**](FundingSourcePurchase.md) | Funding source purchase list | [optional] 


