# MambuApiV2::Asset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Float** | The amount used by the client for the guaranty | 
**deposit_account_key** | **String** | The key of the deposit account used by the guarantor (populated when the guaranty type is GUARANTOR). It can be null. | [optional] 
**asset_name** | **String** | The name of a value the client guarantees with (populated when the guaranty type is ASSET) | 
**encoded_key** | **String** | The encoded key of the security, auto generated, unique. | [optional] 
**guarantor_key** | **String** | The key of the client/group used as the guarantor. | [optional] 
**guarantor_type** | **String** | The type of the guarantor (client/group) | [optional] 


