# MambuApiV2::AccountArrearsSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**monthly_tolerance_day** | **Integer** | Defines monthly arrears tolerance day value. | [optional] 
**tolerance_floor_amount** | **Float** | The tolerance floor amount. | [optional] 
**non_working_days_method** | **String** | Shows whether the non working days are taken in consideration or not when applying penaltees/late fees or when setting an account into arrears | [optional] 
**tolerance_percentage_of_outstanding_principal** | **Float** | Defines the arrears tolerance amount. | [optional] 
**tolerance_period** | **Integer** | Defines the arrears tolerance period value. | [optional] 
**encoded_key** | **String** | The encoded key of the arrears base settings, auto generated, unique. | [optional] 
**tolerance_calculation_method** | **String** | Defines the tolerance calculation method | [optional] 
**date_calculation_method** | **String** | The arrears date calculation method. | [optional] 


