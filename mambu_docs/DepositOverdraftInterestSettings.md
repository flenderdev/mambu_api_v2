# MambuApiV2::DepositOverdraftInterestSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_rate** | **Float** | The interest rate that was set or changed in this transaction. Used on product interest rate changes or interest tier switches | [optional] 
**index_interest_rate** | **Float** | The value of the index interest rate set or changed in this transaction | [optional] 


