# MambuApiV2::LoanAccountAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **String** | The action type to be applied | 
**notes** | **String** | The notes related to the action performed | [optional] 


