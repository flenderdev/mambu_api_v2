# MambuApiV2::RescheduleLoanAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disbursement_details** | [**RescheduleDisbursementDetails**](RescheduleDisbursementDetails.md) | The disbursement details for the rescheduled loan account | 
**notes** | **String** | Notes for the rescheduled loan account | [optional] 
**principal_payment_settings** | [**RestructurePrincipalPaymentAccountSettings**](RestructurePrincipalPaymentAccountSettings.md) | The principal payment settings for the rescheduled loan account | [optional] 
**loan_name** | **String** | The name of the new loan account. | [optional] 
**interest_settings** | [**RestructureInterestSettings**](RestructureInterestSettings.md) | The interest settings for the rescheduled loan account | [optional] 
**schedule_settings** | [**RestructureScheduleSettings**](RestructureScheduleSettings.md) | The schedule settings for the rescheduled loan account | [optional] 
**interest_commission** | **Float** | The interest commission | [optional] 
**encoded_key** | **String** | The encoded key of the entity, generated, globally unique | [optional] 
**product_type_key** | **String** | The key of the loan product that this account is based on | 
**id** | **String** | Id of the new loan account. | [optional] 
**guarantors** | [**Array&lt;Guarantor&gt;**](Guarantor.md) | The guarantees associated with the rescheduled loan account | [optional] 
**account_arrears_settings** | [**RestructureAccountArrearsSettings**](RestructureAccountArrearsSettings.md) | The arrears settings for the rescheduled loan account | [optional] 
**penalty_settings** | [**RestructurePenaltySettings**](RestructurePenaltySettings.md) | The penalty settings for the rescheduled loan account | [optional] 


