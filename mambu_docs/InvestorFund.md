# MambuApiV2::InvestorFund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Float** | The amount used by the client for the guaranty | 
**interest_commission** | **Float** | The constraint minimum value | [optional] 
**deposit_account_key** | **String** | The key of the deposit account used by the guarantor (populated when the guaranty type is GUARANTOR). It can be null. | [optional] 
**asset_name** | **String** | The name of a value the client guarantees with (populated when the guaranty type is ASSET) | [optional] 
**encoded_key** | **String** | The encoded key of the entity, generated, globally unique | [optional] 
**id** | **String** | Investor fund unique identifier. All versions of an investor fund will have same id. | [optional] 
**guarantor_key** | **String** | The key of the client/group used as the guarantor. | 
**guarantor_type** | **String** | The type of the guarantor (client/group) | 
**share_percentage** | **Float** | Percentage of loan shares this investor owns | [optional] 


