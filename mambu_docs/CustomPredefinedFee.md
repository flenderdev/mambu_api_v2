# MambuApiV2::CustomPredefinedFee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**predefined_fee_encoded_key** | **String** | The encoded key of the predefined fee | [optional] 
**encoded_key** | **String** | The encoded key of the custom predefined fee, auto generated, unique. | [optional] 
**amount** | **Float** | The amount of the custom fee. | [optional] 


