# MambuApiV2::DepositTransactionBalances

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_balance** | **Float** | The running balance owed by deposit | [optional] 


