# MambuApiV2::InterestSettingsForSchedulePreview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_spread** | **Float** | Interest to be added to active organization index interest rate in order to find out actual interest rate | [optional] 
**interest_rate** | **Float** | The interest rate. Represents the interest rate for the loan account. The interest on loans is accrued on a daily basis, which allows charging the clients only for the days they actually used the loan amount. | [optional] 


