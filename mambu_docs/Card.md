# MambuApiV2::Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_token** | **String** | The reference token of the card | 


