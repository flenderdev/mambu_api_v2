# MambuApiV2::LoanAccountsApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apply_interest**](LoanAccountsApi.md#apply_interest) | **POST** /loans/{loanAccountId}:applyInterest | Apply accrued interest
[**change_arrears_settings**](LoanAccountsApi.md#change_arrears_settings) | **POST** /loans/{loanAccountId}:changeArrearsSettings | Allows to change the arrears settings for an active loan account
[**change_due_dates_settings**](LoanAccountsApi.md#change_due_dates_settings) | **POST** /loans/{loanAccountId}:changeDueDatesSettings | Allows to change due dates settings for an active loan account
[**change_interest_rate**](LoanAccountsApi.md#change_interest_rate) | **POST** /loans/{loanAccountId}:changeInterestRate | Allows to change the interest rate for a loan account
[**change_periodic_payment**](LoanAccountsApi.md#change_periodic_payment) | **POST** /loans/{loanAccountId}:changePeriodicPayment | Allows to change the periodic payment amount for an active loan, so that we can still be able to have Principal and Interest installments, but with a smaller/greater total due amount than the initial one.
[**change_repayment_value**](LoanAccountsApi.md#change_repayment_value) | **POST** /loans/{loanAccountId}:changeRepaymentValue | Allows to change the repayment value for a loan account
[**change_state**](LoanAccountsApi.md#change_state) | **POST** /loans/{loanAccountId}:changeState | Allows posting an action such as approve/reject/withdraw/close loan account
[**create**](LoanAccountsApi.md#create) | **POST** /loans | Creates a new loan account
[**create_card**](LoanAccountsApi.md#create_card) | **POST** /loans/{loanAccountId}/cards | Create and associate a new card to the provided account
[**delete**](LoanAccountsApi.md#delete) | **DELETE** /loans/{loanAccountId} | Delete a loan account
[**delete_card**](LoanAccountsApi.md#delete_card) | **DELETE** /loans/{loanAccountId}/cards/{cardReferenceToken} | Delete a card associated to the provided account via its reference token
[**get_all**](LoanAccountsApi.md#get_all) | **GET** /loans | Allows retrieval of loan accounts using various query parameters. It&#39;s possible to look up loans by their state, branch, centre or by a credit officer to which the loans are assigned.
[**get_all_authorization_holds**](LoanAccountsApi.md#get_all_authorization_holds) | **GET** /loans/{loanAccountId}/authorizationholds | Retrieves authorization holds related to a loan account, ordered from newest to oldest by creation date
[**get_all_cards**](LoanAccountsApi.md#get_all_cards) | **GET** /loans/{loanAccountId}/cards | Allows retrieval of all cards associated with the account
[**get_by_id**](LoanAccountsApi.md#get_by_id) | **GET** /loans/{loanAccountId} | Allows retrieval of a single loan account via id or encoded key
[**patch**](LoanAccountsApi.md#patch) | **PATCH** /loans/{loanAccountId} | Partially update an existing loan account
[**pay_off**](LoanAccountsApi.md#pay_off) | **POST** /loans/{loanAccountId}:payOff | Allows to pay off a loan account
[**refinance**](LoanAccountsApi.md#refinance) | **POST** /loans/{loanAccountId}:refinance | Allows to refinance a loan account
[**reschedule**](LoanAccountsApi.md#reschedule) | **POST** /loans/{loanAccountId}:reschedule | Allows to reschedule a loan account
[**undo_refinance**](LoanAccountsApi.md#undo_refinance) | **POST** /loans/{loanAccountId}:undoRefinance | Allows to undo refinance a loan account
[**undo_reschedule**](LoanAccountsApi.md#undo_reschedule) | **POST** /loans/{loanAccountId}:undoReschedule | Allows to undo reschedule a loan account
[**undo_write_off**](LoanAccountsApi.md#undo_write_off) | **POST** /loans/{loanAccountId}:undoWriteOff | Undo write off for a loan account
[**update**](LoanAccountsApi.md#update) | **PUT** /loans/{loanAccountId} | Update an existing loan account
[**write_off**](LoanAccountsApi.md#write_off) | **POST** /loans/{loanAccountId}:writeOff | Allows to write off a loan account


# **apply_interest**
> apply_interest(loan_account_id, body, opts)

Apply accrued interest



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

body = MambuApiV2::ApplyInterestInput.new # ApplyInterestInput | Input details for apply accrued interest action

opts = { 
  idempotency_key: 'idempotency_key_example' # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
}

begin
  #Apply accrued interest
  api_instance.apply_interest(loan_account_id, body, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->apply_interest: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **body** | [**ApplyInterestInput**](ApplyInterestInput.md)| Input details for apply accrued interest action | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **change_arrears_settings**
> change_arrears_settings(loan_account_id, opts)

Allows to change the arrears settings for an active loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::ChangeArrearsSettingsInput.new # ChangeArrearsSettingsInput | Allows specifying the action details for a loan account
}

begin
  #Allows to change the arrears settings for an active loan account
  api_instance.change_arrears_settings(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->change_arrears_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**ChangeArrearsSettingsInput**](ChangeArrearsSettingsInput.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **change_due_dates_settings**
> change_due_dates_settings(loan_account_id, opts)

Allows to change due dates settings for an active loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::ChangeDueDatesSettingsInput.new # ChangeDueDatesSettingsInput | Allows specifying the action details for a loan account
}

begin
  #Allows to change due dates settings for an active loan account
  api_instance.change_due_dates_settings(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->change_due_dates_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**ChangeDueDatesSettingsInput**](ChangeDueDatesSettingsInput.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **change_interest_rate**
> change_interest_rate(loan_account_id, opts)

Allows to change the interest rate for a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::ChangeInterestRateLoanAccountInput.new # ChangeInterestRateLoanAccountInput | Allows specifying the action details for a loan account
}

begin
  #Allows to change the interest rate for a loan account
  api_instance.change_interest_rate(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->change_interest_rate: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**ChangeInterestRateLoanAccountInput**](ChangeInterestRateLoanAccountInput.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **change_periodic_payment**
> change_periodic_payment(loan_account_id, opts)

Allows to change the periodic payment amount for an active loan, so that we can still be able to have Principal and Interest installments, but with a smaller/greater total due amount than the initial one.



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::ChangePeriodicPaymentLoanAccountInput.new # ChangePeriodicPaymentLoanAccountInput | Allows specifying the action details for a loan account
}

begin
  #Allows to change the periodic payment amount for an active loan, so that we can still be able to have Principal and Interest installments, but with a smaller/greater total due amount than the initial one.
  api_instance.change_periodic_payment(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->change_periodic_payment: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**ChangePeriodicPaymentLoanAccountInput**](ChangePeriodicPaymentLoanAccountInput.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **change_repayment_value**
> change_repayment_value(loan_account_id, opts)

Allows to change the repayment value for a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::ChangeRepaymentValueLoanAccountInput.new # ChangeRepaymentValueLoanAccountInput | Allows specifying the action details for a loan account
}

begin
  #Allows to change the repayment value for a loan account
  api_instance.change_repayment_value(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->change_repayment_value: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**ChangeRepaymentValueLoanAccountInput**](ChangeRepaymentValueLoanAccountInput.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **change_state**
> LoanAccount change_state(loan_account_id, body, opts)

Allows posting an action such as approve/reject/withdraw/close loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

body = MambuApiV2::LoanAccountAction.new # LoanAccountAction | Allows specifying the action details for a loan account

opts = { 
  idempotency_key: 'idempotency_key_example' # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
}

begin
  #Allows posting an action such as approve/reject/withdraw/close loan account
  result = api_instance.change_state(loan_account_id, body, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->change_state: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **body** | [**LoanAccountAction**](LoanAccountAction.md)| Allows specifying the action details for a loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 

### Return type

[**LoanAccount**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **create**
> LoanAccount create(body, opts)

Creates a new loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

body = MambuApiV2::LoanAccount.new # LoanAccount | Loan account to be created

opts = { 
  idempotency_key: 'idempotency_key_example' # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
}

begin
  #Creates a new loan account
  result = api_instance.create(body, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LoanAccount**](LoanAccount.md)| Loan account to be created | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 

### Return type

[**LoanAccount**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **create_card**
> create_card(loan_account_id, body, opts)

Create and associate a new card to the provided account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

body = MambuApiV2::Card.new # Card | Card to be created

opts = { 
  idempotency_key: 'idempotency_key_example' # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
}

begin
  #Create and associate a new card to the provided account
  api_instance.create_card(loan_account_id, body, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->create_card: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **body** | [**Card**](Card.md)| Card to be created | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **delete**
> delete(loan_account_id)

Delete a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account


begin
  #Delete a loan account
  api_instance.delete(loan_account_id)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **delete_card**
> delete_card(loan_account_id, card_reference_token)

Delete a card associated to the provided account via its reference token



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

card_reference_token = 'card_reference_token_example' # String | Reference token of the card to be retrieved


begin
  #Delete a card associated to the provided account via its reference token
  api_instance.delete_card(loan_account_id, card_reference_token)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->delete_card: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **card_reference_token** | **String**| Reference token of the card to be retrieved | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **get_all**
> Array&lt;LoanAccount&gt; get_all(opts)

Allows retrieval of loan accounts using various query parameters. It's possible to look up loans by their state, branch, centre or by a credit officer to which the loans are assigned.



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

opts = { 
  offset: 56, # Integer | Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results
  limit: 56, # Integer | Pagination, the number of elements to retrieve, used in combination with offset to paginate results
  pagination_details: 'OFF', # String | Flag specifying whether the pagination  details should be provided in response headers. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs
  details_level: 'details_level_example', # String | The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object
  credit_officer_username: 'credit_officer_username_example', # String | The username of the credit officer to whom the entities are assigned to
  branch_id: 'branch_id_example', # String | The id/encodedKey of the branch to which the entities are assigned to
  centre_id: 'centre_id_example', # String | The id/encodedKey of the centre to which the entities are assigned to
  account_state: 'account_state_example', # String | The state of the loan account to search for
  account_holder_type: 'account_holder_type_example', # String | The type of the account holder: CLIENT/GROUP
  account_holder_id: 'account_holder_id_example', # String | The id of the account holder
  sort_by: 'sort_by_example' # String | The criteria based on which the records will be sorted. Expected format is <field:order>, eg sortBy = field1:ASC,field2:DESC.<br/>Only the following fields can be used: id, loanName, creationDate, lastModifiedDate<br/>Default sorting is done by creationDate:ASC
}

begin
  #Allows retrieval of loan accounts using various query parameters. It's possible to look up loans by their state, branch, centre or by a credit officer to which the loans are assigned.
  result = api_instance.get_all(opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->get_all: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **Integer**| Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results | [optional] 
 **limit** | **Integer**| Pagination, the number of elements to retrieve, used in combination with offset to paginate results | [optional] 
 **pagination_details** | **String**| Flag specifying whether the pagination  details should be provided in response headers. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs | [optional] [default to OFF]
 **details_level** | **String**| The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object | [optional] 
 **credit_officer_username** | **String**| The username of the credit officer to whom the entities are assigned to | [optional] 
 **branch_id** | **String**| The id/encodedKey of the branch to which the entities are assigned to | [optional] 
 **centre_id** | **String**| The id/encodedKey of the centre to which the entities are assigned to | [optional] 
 **account_state** | **String**| The state of the loan account to search for | [optional] 
 **account_holder_type** | **String**| The type of the account holder: CLIENT/GROUP | [optional] 
 **account_holder_id** | **String**| The id of the account holder | [optional] 
 **sort_by** | **String**| The criteria based on which the records will be sorted. Expected format is &lt;field:order&gt;, eg sortBy &#x3D; field1:ASC,field2:DESC.&lt;br/&gt;Only the following fields can be used: id, loanName, creationDate, lastModifiedDate&lt;br/&gt;Default sorting is done by creationDate:ASC | [optional] 

### Return type

[**Array&lt;LoanAccount&gt;**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **get_all_authorization_holds**
> Array&lt;GetAuthorizationHold&gt; get_all_authorization_holds(loan_account_id, opts)

Retrieves authorization holds related to a loan account, ordered from newest to oldest by creation date



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  offset: 56, # Integer | Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results
  limit: 56, # Integer | Pagination, the number of elements to retrieve, used in combination with offset to paginate results
  pagination_details: 'OFF', # String | Flag specifying whether the pagination  details should be provided in response headers. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs
  status: 'status_example' # String | The status of the authorization holds to filter on
}

begin
  #Retrieves authorization holds related to a loan account, ordered from newest to oldest by creation date
  result = api_instance.get_all_authorization_holds(loan_account_id, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->get_all_authorization_holds: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **offset** | **Integer**| Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results | [optional] 
 **limit** | **Integer**| Pagination, the number of elements to retrieve, used in combination with offset to paginate results | [optional] 
 **pagination_details** | **String**| Flag specifying whether the pagination  details should be provided in response headers. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs | [optional] [default to OFF]
 **status** | **String**| The status of the authorization holds to filter on | [optional] 

### Return type

[**Array&lt;GetAuthorizationHold&gt;**](GetAuthorizationHold.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **get_all_cards**
> Array&lt;Card&gt; get_all_cards(loan_account_id)

Allows retrieval of all cards associated with the account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account


begin
  #Allows retrieval of all cards associated with the account
  result = api_instance.get_all_cards(loan_account_id)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->get_all_cards: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 

### Return type

[**Array&lt;Card&gt;**](Card.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **get_by_id**
> LoanAccount get_by_id(loan_account_id, opts)

Allows retrieval of a single loan account via id or encoded key



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  details_level: 'details_level_example' # String | The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object
}

begin
  #Allows retrieval of a single loan account via id or encoded key
  result = api_instance.get_by_id(loan_account_id, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->get_by_id: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **details_level** | **String**| The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object | [optional] 

### Return type

[**LoanAccount**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **patch**
> patch(loan_account_id, body)

Partially update an existing loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

body = [MambuApiV2::PatchOperation.new] # Array<PatchOperation> | Patch operations to be applied to a resource


begin
  #Partially update an existing loan account
  api_instance.patch(loan_account_id, body)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->patch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **body** | [**Array&lt;PatchOperation&gt;**](PatchOperation.md)| Patch operations to be applied to a resource | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **pay_off**
> pay_off(loan_account_id, body, opts)

Allows to pay off a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

body = MambuApiV2::LoanAccountPayOffInput.new # LoanAccountPayOffInput | Allows specifying the input for loan account pay off

opts = { 
  idempotency_key: 'idempotency_key_example' # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
}

begin
  #Allows to pay off a loan account
  api_instance.pay_off(loan_account_id, body, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->pay_off: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **body** | [**LoanAccountPayOffInput**](LoanAccountPayOffInput.md)| Allows specifying the input for loan account pay off | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **refinance**
> LoanAccount refinance(loan_account_id, opts)

Allows to refinance a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::RefinanceLoanAccountAction.new # RefinanceLoanAccountAction | Allows specifying the action details for a loan account
}

begin
  #Allows to refinance a loan account
  result = api_instance.refinance(loan_account_id, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->refinance: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**RefinanceLoanAccountAction**](RefinanceLoanAccountAction.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

[**LoanAccount**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **reschedule**
> LoanAccount reschedule(loan_account_id, opts)

Allows to reschedule a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::RescheduleLoanAccountAction.new # RescheduleLoanAccountAction | Allows specifying the action details for a loan account
}

begin
  #Allows to reschedule a loan account
  result = api_instance.reschedule(loan_account_id, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->reschedule: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**RescheduleLoanAccountAction**](RescheduleLoanAccountAction.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

[**LoanAccount**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **undo_refinance**
> undo_refinance(loan_account_id, opts)

Allows to undo refinance a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::LoanActionDetails.new # LoanActionDetails | Allows specifying the action details for a loan account
}

begin
  #Allows to undo refinance a loan account
  api_instance.undo_refinance(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->undo_refinance: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**LoanActionDetails**](LoanActionDetails.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **undo_reschedule**
> undo_reschedule(loan_account_id, opts)

Allows to undo reschedule a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::LoanActionDetails.new # LoanActionDetails | Allows specifying the action details for a loan account
}

begin
  #Allows to undo reschedule a loan account
  api_instance.undo_reschedule(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->undo_reschedule: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**LoanActionDetails**](LoanActionDetails.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **undo_write_off**
> undo_write_off(loan_account_id, body, opts)

Undo write off for a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

body = MambuApiV2::LoanActionDetails.new # LoanActionDetails |  Allows to reverse writeOff for a loan account

opts = { 
  idempotency_key: 'idempotency_key_example' # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
}

begin
  #Undo write off for a loan account
  api_instance.undo_write_off(loan_account_id, body, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->undo_write_off: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **body** | [**LoanActionDetails**](LoanActionDetails.md)|  Allows to reverse writeOff for a loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **update**
> LoanAccount update(loan_account_id, body)

Update an existing loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

body = MambuApiV2::LoanAccount.new # LoanAccount | Loan account to be updated


begin
  #Update an existing loan account
  result = api_instance.update(loan_account_id, body)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **body** | [**LoanAccount**](LoanAccount.md)| Loan account to be updated | 

### Return type

[**LoanAccount**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



# **write_off**
> write_off(loan_account_id, opts)

Allows to write off a loan account



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

opts = { 
  idempotency_key: 'idempotency_key_example', # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
  body: MambuApiV2::LoanActionDetails.new # LoanActionDetails | Allows specifying the action details for a loan account
}

begin
  #Allows to write off a loan account
  api_instance.write_off(loan_account_id, opts)
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsApi->write_off: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 
 **body** | [**LoanActionDetails**](LoanActionDetails.md)| Allows specifying the action details for a loan account | [optional] 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



