# MambuApiV2::RescheduleLoanAccountAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**write_off_amounts** | [**RescheduleWriteOffAmounts**](RescheduleWriteOffAmounts.md) | The write-off amounts | [optional] 
**loan_account** | [**RescheduleLoanAccount**](RescheduleLoanAccount.md) | The rescheduled loan account details | 


