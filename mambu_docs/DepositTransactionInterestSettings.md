# MambuApiV2::DepositTransactionInterestSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_rate** | **Float** | The interest rate for the deposit account | [optional] 


