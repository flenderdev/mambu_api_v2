# MambuApiV2::ChangeRepaymentValueLoanAccountInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Float** | Fixed amount for being used for the repayments principal due | [optional] 
**value_date** | **DateTime** | Date when to change the repayment value (as Organization Time) | 
**notes** | **String** | Notes for the repayment value change action performed on the loan account | [optional] 
**percentage** | **Float** | Percentage of principal amount used for the repayments principal due | [optional] 


