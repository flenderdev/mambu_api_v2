# MambuApiV2::ScheduleSettingsForSchedulePreview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**principal_repayment_interval** | **Integer** | The principal repayment interval. Indicates the interval of repayments that the principal has to be paid. | [optional] 
**grace_period** | **Integer** | The grace period. Represents the grace period for loan repayment - in number of installments. | [optional] 
**periodic_payment** | **Float** | The periodic payment amount for the accounts which have balloon payments or Reduce Number of Installments and Optimized Payments | [optional] 
**repayment_period_unit** | **String** | The repayment period unit. Represents the frequency of loan repayment. | [optional] 
**payment_plan** | [**Array&lt;PeriodicPaymentForSchedulePreview&gt;**](PeriodicPaymentForSchedulePreview.md) | A list of periodic payments for the current loan account. | [optional] 
**fixed_days_of_month** | **Array&lt;Integer&gt;** | Specifies the days of the month when the repayment due dates should be. Only available if the Repayment Methodology is FIXED_DAYS_OF_MONTH. | [optional] 
**repayment_installments** | **Integer** | The repayment installments. Represents how many installments are required to pay back the loan. | [optional] 
**repayment_period_count** | **Integer** | The repayment period count. Represents how often the loan is to be repaid: stored based on the type repayment option. | [optional] 


