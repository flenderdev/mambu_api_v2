# MambuApiV2::Agent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**financial_institution_identification** | [**FinancialInstitutionIdentification**](FinancialInstitutionIdentification.md) | The identification of the financial institution servicing an account | [optional] 


