# MambuApiV2::LoanTranche

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encoded_key** | **String** | The encoded key of the transaction details , auto generated, unique. | [optional] 
**amount** | **Float** | The amount this tranche has available for disburse | 
**fees** | [**Array&lt;CustomPredefinedFee&gt;**](CustomPredefinedFee.md) | Fees that are associated with this tranche | [optional] 
**disbursement_details** | [**TrancheDisbursementDetails**](TrancheDisbursementDetails.md) | The disbursement details of this tranche | [optional] 
**tranche_number** | **Integer** | Index indicating the tranche number | [optional] 


