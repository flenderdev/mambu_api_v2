# MambuApiV2::RemittanceInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**structured** | [**Structured**](Structured.md) | Information supplied to match the items of the payment in a structured form | [optional] 
**unstructured** | **String** | Information supplied to match the items of the payment in an unstructured form | [optional] 


