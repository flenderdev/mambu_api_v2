# MambuApiV2::PeriodicPaymentForSchedulePreview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to_installment** | **Integer** | The installment&#39;s position up to which the PMT will be used | 
**amount** | **Float** | The PMT value used in periodic payment | 


