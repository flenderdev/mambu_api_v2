# MambuApiV2::InstallmentAllocationElementTaxableAmount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**Amount**](Amount.md) | The installment allocation element amount. | [optional] 
**tax** | [**Amount**](Amount.md) | The taxes amount on the income from the installment allocation element amount. | [optional] 


