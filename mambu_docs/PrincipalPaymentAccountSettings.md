# MambuApiV2::PrincipalPaymentAccountSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**include_interest_in_floor_amount** | **BOOLEAN** | Boolean flag, if true, the interest will be included along with the principal in the repayment floor amount, for a revolving credit account | [optional] 
**total_due_payment** | **String** | The method of total due payment for revolving credit | [optional] 
**amount** | **Float** | Fixed amount for being used for the repayments principal due. | [optional] 
**principal_floor_value** | **Float** | The minimum principal due amount a repayment made with this settings can have | [optional] 
**principal_payment_method** | **String** | The method of principal payment for revolving credit. | [optional] 
**percentage** | **Float** | Percentage of principal amount used for the repayments principal due. | [optional] 
**include_fees_in_floor_amount** | **BOOLEAN** | Boolean flag, if true, the fees will be included along with the principal in the repayment floor amount, for a revolving credit account | [optional] 
**encoded_key** | **String** | The encoded key of the principal payment base settings, auto generated, unique. | [optional] 
**total_due_amount_floor** | **Float** | The minimum total due amount a repayment made with this settings can have | [optional] 
**principal_ceiling_value** | **Float** | The maximum principal due amount a repayment made with this settings can have | [optional] 


