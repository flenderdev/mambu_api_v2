# MambuApiV2::FinancialInstitutionIdentification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bic** | **String** | Business identifier code | [optional] 


