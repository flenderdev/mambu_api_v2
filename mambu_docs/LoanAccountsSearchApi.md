# MambuApiV2::LoanAccountsSearchApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search**](LoanAccountsSearchApi.md#search) | **POST** /loans:search | Client Directed Query. Allows you to search loan accounts by various criteria


# **search**
> Array&lt;LoanAccount&gt; search(body, opts)

Client Directed Query. Allows you to search loan accounts by various criteria



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountsSearchApi.new

body = MambuApiV2::LoanAccountSearchCriteria.new # LoanAccountSearchCriteria | Criteria to be used to search the loan accounts

opts = { 
  offset: 56, # Integer | Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results
  limit: 56, # Integer | Pagination, the number of elements to retrieve, used in combination with offset to paginate results
  pagination_details: 'OFF', # String | Flag specifying whether the pagination  details should be provided in response headers. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs
  details_level: 'details_level_example' # String | The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object
}

begin
  #Client Directed Query. Allows you to search loan accounts by various criteria
  result = api_instance.search(body, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountsSearchApi->search: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LoanAccountSearchCriteria**](LoanAccountSearchCriteria.md)| Criteria to be used to search the loan accounts | 
 **offset** | **Integer**| Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results | [optional] 
 **limit** | **Integer**| Pagination, the number of elements to retrieve, used in combination with offset to paginate results | [optional] 
 **pagination_details** | **String**| Flag specifying whether the pagination  details should be provided in response headers. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs | [optional] [default to OFF]
 **details_level** | **String**| The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object | [optional] 

### Return type

[**Array&lt;LoanAccount&gt;**](LoanAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



