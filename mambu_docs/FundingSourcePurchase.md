# MambuApiV2::FundingSourcePurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deposit_account_key** | **String** | The buyer funding account (savings account) key | [optional] 
**amount** | **Float** | The amount bought (a portion of the whole fraction being sold) | 
**price** | **Float** | The price paid for the amount | 


