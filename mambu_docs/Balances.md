# MambuApiV2::Balances

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_from_arrears_paid** | **Float** | The interest from arrears paid, indicates total interest from arrears paid into the account. | [optional] 
**principal_due** | **Float** | The principal due, indicates how much principal it&#39;s due at this moment. | [optional] 
**interest_balance** | **Float** | The interest balance. Represents the total interest owned by the client, from now on (total interest accrued for account - interest paid). | [optional] 
**hold_balance** | **Float** | The sum of all the authorization hold amounts on this account. | [optional] 
**principal_paid** | **Float** | The principal paid, holds the value of the total paid into the account. | [optional] 
**penalty_due** | **Float** | The penalty due. Represents the total penalty amount due for the account. | [optional] 
**fees_balance** | **Float** | The fees balance. Represents the total fees expected to be paid on this account at a given moment. | [optional] 
**penalty_balance** | **Float** | The penalty balance. Represents the total penalty expected to be paid on this account at a given moment. | [optional] 
**redraw_balance** | **Float** | The total redraw amount owned by the client, from now on. | [optional] 
**interest_from_arrears_balance** | **Float** | The interest from arrears balance. Indicates interest from arrears owned by the client, from now on. (total interest from arrears accrued for account - interest from arrears paid). | [optional] 
**principal_balance** | **Float** | The total principal owned by the client, from now on (principal disbursed - principal paid). | [optional] 
**interest_due** | **Float** | The interest due. Indicates how much interest it&#39;s due for the account at this moment. | [optional] 
**penalty_paid** | **Float** | The Penalty paid. Represents the total penalty amount paid for the account. | [optional] 
**fees_paid** | **Float** | The fees paid. Represents the total fees paid for the account. | [optional] 
**interest_from_arrears_due** | **Float** | The interest from arrears due. Indicates how much interest from arrears it&#39;s due for the account at this moment. | [optional] 
**fees_due** | **Float** | The fees due. Representing the total fees due for the account. | [optional] 
**interest_paid** | **Float** | The interest paid, indicates total interest paid into the account. | [optional] 


