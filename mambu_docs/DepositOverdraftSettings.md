# MambuApiV2::DepositOverdraftSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**overdraft_limit** | **Float** | The overdraft limit that was set or changed in this transaction | [optional] 


