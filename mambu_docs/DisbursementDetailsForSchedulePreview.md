# MambuApiV2::DisbursementDetailsForSchedulePreview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expected_disbursement_date** | **DateTime** | The date of the expected disbursement.Stored as Organization Time. | [optional] 
**first_repayment_date** | **DateTime** | The date of the expected first repayment. Stored as Organization Time. | [optional] 


