# MambuApiV2::Structured

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creditor_reference_information** | [**CreditorReferenceInformation**](CreditorReferenceInformation.md) | Information supplied to match the items of the payment in a structured form for the creditor | [optional] 


