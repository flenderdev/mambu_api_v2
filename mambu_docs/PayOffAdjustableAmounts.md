# MambuApiV2::PayOffAdjustableAmounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fees_paid** | **Float** | The fee amount to be paid for Pay Off action | [optional] 
**penalty_paid** | **Float** | The penalty amount to be paid for Pay Off action | [optional] 
**interest_paid** | **Float** | The interest amount to be paid for Pay Off action | [optional] 


