# MambuApiV2::PaymentDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_identification** | [**PaymentIdentification**](PaymentIdentification.md) | Identifiers of a payment instruction | [optional] 
**debtor_account** | [**AccountDetails**](AccountDetails.md) | Includes the debtor account details like identification and currency | [optional] 
**debtor** | [**Party**](Party.md) | Includes the details of the party that owes an amount of money to the creditor | [optional] 
**debtor_agent** | [**Agent**](Agent.md) | Includes the details of the financial institution servicing an account for the debtor | [optional] 
**creditor_agent** | [**Agent**](Agent.md) | Includes the details of the financial institution servicing an account for the creditor | [optional] 
**remittance_information** | [**RemittanceInformation**](RemittanceInformation.md) | Information supplied to match the items of the payment | [optional] 
**creditor_account** | [**AccountDetails**](AccountDetails.md) | Includes the creditor account details like identification and currency | [optional] 
**creditor** | [**Party**](Party.md) | Includes the details of the party to which an amount of money is due | [optional] 
**payment_type_information** | [**PaymentTypeInformation**](PaymentTypeInformation.md) | The information specifying the type of transaction | [optional] 


