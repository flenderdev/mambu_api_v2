# MambuApiV2::ApplyInterestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_holidays_interest_amount** | **Float** | The amount of the Payment Holidays interest to apply | [optional] 
**interest_application_date** | **DateTime** | The date up to which interest is to be posted | 
**notes** | **String** | Additional information for this action | [optional] 
**is_payment_holidays_interest** | **BOOLEAN** | Whether the interest amount to apply should be the regular one or the one accrued during the Payment Holidays. If nothing specified it will be the regular one. | [optional] 


