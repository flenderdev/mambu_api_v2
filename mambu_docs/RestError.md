# MambuApiV2::RestError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_code** | **Integer** |  | [optional] 
**error_source** | **String** |  | [optional] 
**error_reason** | **String** |  | [optional] 


