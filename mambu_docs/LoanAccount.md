# MambuApiV2::LoanAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_state** | **String** | The state of the loan account | [optional] 
**prepayment_settings** | [**PrepaymentSettings**](PrepaymentSettings.md) | The prepayment settings details of the loan. | [optional] 
**migration_event_key** | **String** | The migration event encoded key associated with this loan account. If this account was imported, track which &#39;migration event&#39; they came from. | [optional] 
**last_set_to_arrears_date** | **DateTime** | Date when the loan account was set to last standing, or null if never set (as Organization Time) | [optional] 
**notes** | **String** | Extra notes about this loan account. | [optional] 
**disbursement_details** | [**DisbursementDetails**](DisbursementDetails.md) | Details regarding the disbursement | [optional] 
**redraw_settings** | [**LoanAccountRedrawSettings**](LoanAccountRedrawSettings.md) | The redraw settings, holds information about redraw. | [optional] 
**days_in_arrears** | **Integer** | The number of days in arrears | [optional] 
**account_sub_state** | **String** | This field holds a second state for the account. Beside the account state, we might need to hold sometimes a different information to actually represent the correct life-cycle step in which the account is at that moment. The account behaves as the main state implies, but it  can have either some limitations either some extra behavior rules. For example, even if the account is Active, it can also be Locked in the same time which impliessome limitations over the actions which can be performed with the account. | [optional] 
**loan_name** | **String** | The name of the loan account. | [optional] 
**interest_settings** | [**InterestSettings**](InterestSettings.md) | The interest settings details for this loan. | [optional] 
**assets** | [**Array&lt;Asset&gt;**](Asset.md) | A list of assets associated with the current loan account | [optional] 
**last_interest_review_date** | **DateTime** | The last interest review date. Indicates the date the interest was reviewed last time. Stored as Organization Time. | [optional] 
**id** | **String** | The id of the loan, can be generated and customized, unique | [optional] 
**assigned_user_key** | **String** | Key of the user this loan is assigned to | [optional] 
**future_payments_acceptance** | **String** | Shows whether the future payments are allowed or not for this account (repayment transactions with entry date set in the future) | [optional] 
**original_account_key** | **String** | Key of the original rescheduled/refinanced account | [optional] 
**locked_operations** | **Array&lt;String&gt;** | A list with operations which are locked when the account is in sub-state AccountState.LOCKED. | [optional] 
**accrued_interest** | **Float** | The accrued interest. Represents the amount of interest that has been accrued in the account. | [optional] 
**accrued_penalty** | **Float** | The accrued penalty, represents the amount of penalty that has been accrued in the account. | [optional] 
**creation_date** | **DateTime** | The date this loan account was created | [optional] 
**assigned_centre_key** | **String** | Key of the centre this account is assigned to | [optional] 
**tranches** | [**Array&lt;LoanTranche&gt;**](LoanTranche.md) | A list of disbursement tranches available for the current loan account. | [optional] 
**approved_date** | **DateTime** | The date this loan account was approved | [optional] 
**tax_rate** | **Float** | The tax rate. | [optional] 
**last_tax_rate_review_date** | **DateTime** | The last tax rate review date. Indicates When/if the account had last tax rate checked (as Organization Time). | [optional] 
**interest_from_arrears_accrued** | **Float** | The interest from arrears accrued. Represents the amount of interest from arrears that has been accrued in the account. | [optional] 
**schedule_settings** | [**ScheduleSettings**](ScheduleSettings.md) | The schedule settings details for this loan. | 
**days_late** | **Integer** | The number of days late | [optional] 
**payment_method** | **String** |  The payment method. Represents the interest payment method that determines whether the payments are made Horizontally (on the Repayments) or Vertically (on the Loan Account) | [optional] 
**account_holder_key** | **String** | The encodedKey of the client (a.k.a account holder) | 
**late_payments_recalculation_method** | **String** | Overdue payments recalculation method copied from the loan product on which this account is based | [optional] 
**funding_sources** | [**Array&lt;InvestorFund&gt;**](InvestorFund.md) | A list of funds associated with the current loan account | [optional] 
**account_holder_type** | **String** | The type of the account holder (i.e CLIENT) | 
**arrears_tolerance_period** | **Integer** | The arrears tolerance (period or day of month) depending on the product settings | [optional] 
**last_interest_applied_date** | **DateTime** | The last interest applied date. Indicates when/if the account had last interest applied (stored to interest balance) (as Organization Time) | [optional] 
**rescheduled_account_key** | **String** | Key pointing to where this account was rescheduled/refinanced to. only not null if rescheduled | [optional] 
**payment_holidays_accrued_interest** | **Float** | The Payment Holidays interest accrued. Represents the amount of interest that has been accrued during Payment Holidays in the account. | [optional] 
**activation_transaction_key** | **String** | The encoded key of the transaction that activated this account | [optional] 
**assigned_branch_key** | **String** | Key of the branch this loan account is assigned to. Loan account&#39;s branch is set to unassigned if no branch field is set | [optional] 
**balances** | [**Balances**](Balances.md) | Balances details for this loan account | [optional] 
**credit_arrangement_key** | **String** | The key to the line of credit where this account is registered to. | [optional] 
**interest_commission** | **Float** | The value of the interest booked by the organization from the accounts funded by investors. Null if the funds are not enable | [optional] 
**encoded_key** | **String** | The encoded key of the loan account, auto generated, unique | [optional] 
**last_account_appraisal_date** | **DateTime** | When/if the account had last been evaluated for interest, principal, fees and penalties calculations (as Organization Time) | [optional] 
**penalty_settings** | [**PenaltySettings**](PenaltySettings.md) | The penalty settings details of the loan. | [optional] 
**settlement_account_key** | **String** | The encodedKey of the settlement account | [optional] 
**last_modified_date** | **DateTime** | The last date the loan was updated | [optional] 
**principal_payment_settings** | [**PrincipalPaymentAccountSettings**](PrincipalPaymentAccountSettings.md) | Defines the settings for Revolving Credit principal payments | [optional] 
**last_locked_date** | **DateTime** | Holds the date when the account was set for the last time in the LOCKED state. If null, the account is not locked anymore. Stored as Organization Time | [optional] 
**loan_amount** | **Float** | The loan amount | 
**closed_date** | **DateTime** | The date this loan was closed | [optional] 
**product_type_key** | **String** | The key to the type of product that this account is based on. | 
**allow_offset** | **BOOLEAN** | DEPRECATED - Will always be false. | [optional] 
**guarantors** | [**Array&lt;Guarantor&gt;**](Guarantor.md) | A list of guarantees associated with the current loan account | [optional] 
**account_arrears_settings** | [**AccountArrearsSettings**](AccountArrearsSettings.md) | Defines the account&#39;s arrears settings | [optional] 


