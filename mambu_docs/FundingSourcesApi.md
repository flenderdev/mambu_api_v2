# MambuApiV2::FundingSourcesApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sell**](FundingSourcesApi.md#sell) | **POST** /fundingsources/{fundingSourceId}:sell | Performs the sell of a funding share owned by an investor. Investors can sell the total share or only a part of the investment. In case of a partial sale, multiple operations can be performed until the entire investment is sold. For the seller, money will be deposited in the funding account, for the buyers money will be withdrawn from provided accounts.


# **sell**
> Array&lt;DepositTransaction&gt; sell(funding_source_id, body, opts)

Performs the sell of a funding share owned by an investor. Investors can sell the total share or only a part of the investment. In case of a partial sale, multiple operations can be performed until the entire investment is sold. For the seller, money will be deposited in the funding account, for the buyers money will be withdrawn from provided accounts.



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::FundingSourcesApi.new

funding_source_id = 'funding_source_id_example' # String | Id/Encoded key of the funding source

body = MambuApiV2::SellFundingSourceAction.new # SellFundingSourceAction | List of purchases containing details about buyer, price, amount

opts = { 
  idempotency_key: 'idempotency_key_example' # String | Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting.
}

begin
  #Performs the sell of a funding share owned by an investor. Investors can sell the total share or only a part of the investment. In case of a partial sale, multiple operations can be performed until the entire investment is sold. For the seller, money will be deposited in the funding account, for the buyers money will be withdrawn from provided accounts.
  result = api_instance.sell(funding_source_id, body, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling FundingSourcesApi->sell: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **funding_source_id** | **String**| Id/Encoded key of the funding source | 
 **body** | [**SellFundingSourceAction**](SellFundingSourceAction.md)| List of purchases containing details about buyer, price, amount | 
 **idempotency_key** | **String**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | [optional] 

### Return type

[**Array&lt;DepositTransaction&gt;**](DepositTransaction.md)

### Authorization

[basic](../lib/mambu_api_v2/README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



