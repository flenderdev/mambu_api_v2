# MambuApiV2::RestructurePrincipalPaymentAccountSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Float** | Fixed principal payment amount | [optional] 
**percentage** | **Float** | Principal payment percentage | [optional] 


