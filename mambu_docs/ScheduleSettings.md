# MambuApiV2::ScheduleSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grace_period** | **Integer** | The grace period. Represents the grace period for loan repayment - in number of installments. | 
**periodic_payment** | **Float** | The periodic payment amount for the accounts which have balloon payments or Reduce Number of Installments and Optimized Payments | [optional] 
**repayment_schedule_method** | **String** | The repayment schedule method. Represents the method that determines whether the schedule will be fixed all over the loan account&#39;s life cycle or will be dynamically recomputed when required. | [optional] 
**payment_plan** | [**Array&lt;PeriodicPayment&gt;**](PeriodicPayment.md) | A list of periodic payments for the current loan account. | [optional] 
**short_month_handling_method** | **String** | The short handling method. Determines how to handle the short months, if they select a fixed day of month &gt; 28. Will be null if no such date is selected and also for the Interval methodology. Only available if the Repayment Methodology is FIXED_DAYS_OF_MONTH. | [optional] 
**repayment_installments** | **Integer** | The repayment installments. Represents how many installments are required to pay back the loan. | [optional] 
**grace_period_type** | **String** | The grace period type. Representing the type of grace period which is possible for a loan account. | [optional] 
**principal_repayment_interval** | **Integer** | The principal repayment interval. Indicates the interval of repayments that the principal has to be paid. | [optional] 
**has_custom_schedule** | **BOOLEAN** | Flag used when the repayments schedule for the current account was determined by the user, by editing the due dates or the principal due | [optional] 
**repayment_period_unit** | **String** | The repayment period unit. Represents the frequency of loan repayment. | [optional] 
**fixed_days_of_month** | **Array&lt;Integer&gt;** | Specifies the days of the month when the repayment due dates should be. Only available if the Repayment Methodology is FIXED_DAYS_OF_MONTH. | [optional] 
**schedule_due_dates_method** | **String** | The schedule due dates method. Represents the methodology used by this account to compute the due dates of the repayments. | [optional] 
**repayment_period_count** | **Integer** | The repayment period count. Represents how often the loan is to be repaid: stored based on the type repayment option. | [optional] 
**default_first_repayment_due_date_offset** | **Integer** | The default first repayment due date offset, indicates how many days the first repayment due date should be extended(all other due dates from the schedule are relative to first repayment due date - they will also be affected by the offset) | [optional] 


