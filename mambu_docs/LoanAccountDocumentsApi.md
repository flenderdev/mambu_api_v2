# MambuApiV2::LoanAccountDocumentsApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_loan_account_document**](LoanAccountDocumentsApi.md#get_loan_account_document) | **GET** /loans/{loanAccountId}/templates/{templateId} | Allows retrieval of a loan account document (populated template) by provided loan account id and template id


# **get_loan_account_document**
> String get_loan_account_document(loan_account_id, template_id, opts)

Allows retrieval of a loan account document (populated template) by provided loan account id and template id



### Example
```ruby
# load the gem
require 'mambu_api_v2'
# setup authorization
MambuApiV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = MambuApiV2::LoanAccountDocumentsApi.new

loan_account_id = 'loan_account_id_example' # String | The id or encoded key of the loan account

template_id = 'template_id_example' # String | The id of the loan product template

opts = { 
  start_date: Date.parse('2013-10-20'), # Date | The first date to consider when the document contains a list of transactions. Required when document contains a transaction history.
  end_date: Date.parse('2013-10-20') # Date | The last date to consider when the document contains a list of transactions. Required when document contains a transaction history.
}

begin
  #Allows retrieval of a loan account document (populated template) by provided loan account id and template id
  result = api_instance.get_loan_account_document(loan_account_id, template_id, opts)
  p result
rescue MambuApiV2::ApiError => e
  puts "Exception when calling LoanAccountDocumentsApi->get_loan_account_document: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loan_account_id** | **String**| The id or encoded key of the loan account | 
 **template_id** | **String**| The id of the loan product template | 
 **start_date** | **Date**| The first date to consider when the document contains a list of transactions. Required when document contains a transaction history. | [optional] 
 **end_date** | **Date**| The last date to consider when the document contains a list of transactions. Required when document contains a transaction history. | [optional] 

### Return type

**String**

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json



