# MambuApiV2::PatchOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**op** | **String** | The change to perform | 
**path** | **String** | The field to perform the operation on | 
**from** | **String** | The field from where a value should be moved, when using move | [optional] 
**value** | **Object** | The value of the field, can be null | [optional] 


