# MambuApiV2::OtherAccountIdentification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scheme** | **String** | The identification scheme | [optional] 
**identification** | **String** | The identification of the payer/payee | [optional] 


