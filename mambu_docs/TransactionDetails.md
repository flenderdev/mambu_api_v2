# MambuApiV2::TransactionDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_channel_id** | **String** | The id of the transaction channel associated with the transaction details. | [optional] 
**transaction_channel_key** | **String** | The encoded key of the transaction channel associated with the transaction details. | [optional] 


