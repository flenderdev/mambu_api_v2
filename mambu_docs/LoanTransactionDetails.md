# MambuApiV2::LoanTransactionDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encoded_key** | **String** | The encoded key of the entity, generated, globally unique | [optional] 
**internal_transfer** | **BOOLEAN** | Whether the transaction was transferred between loans or deposit accounts | [optional] 
**transaction_channel_key** | **String** | The encoded key of the transaction channel associated with the transaction details. | [optional] 
**transaction_channel_id** | **String** | The id of the transaction channel associated with the transaction details. | [optional] 
**target_deposit_account_key** | **String** | In case of a transaction to a deposit account this represent the deposit account key to which the transaction was made. | [optional] 


