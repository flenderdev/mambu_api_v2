# MambuApiV2::RestructureInterestSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest_rate** | **Float** | The interest rate for the restructured loan account | [optional] 
**interest_spread** | **Float** | The interest spread for the restructured loan account | [optional] 


