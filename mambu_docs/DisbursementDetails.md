# MambuApiV2::DisbursementDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_details** | [**LoanTransactionDetails**](LoanTransactionDetails.md) | The input details for the disbursement transaction | [optional] 
**expected_disbursement_date** | **DateTime** | The date of the expected disbursement.Stored as Organization Time. | [optional] 
**fees** | [**Array&lt;CustomPredefinedFee&gt;**](CustomPredefinedFee.md) | List of fees that should be applied at the disbursement time. | [optional] 
**first_repayment_date** | **DateTime** | The date of the expected first repayment. Stored as Organization Time. | [optional] 
**disbursement_date** | **DateTime** | The activation date, the date when the disbursement actually took place. | [optional] 
**encoded_key** | **String** | The encoded key of the disbursement details, auto generated, unique | [optional] 


