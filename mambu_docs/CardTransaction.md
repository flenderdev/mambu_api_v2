# MambuApiV2::CardTransaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_reference_id** | **String** | The external reference ID to be used to reference the card transaction in subsequent requests. | 
**amount** | **Float** | The amount of money to be withdrawn in the financial transaction. | 
**advice** | **BOOLEAN** | Whether the given request should be accepted without balance validations. | 
**external_authorization_reference_id** | **String** | The external authorization hold reference ID, which relates this card transaction to a previous authorization hold. | [optional] 
**card_acceptor** | [**CardAcceptor**](CardAcceptor.md) | The card acceptor details. | [optional] 
**encoded_key** | **String** | The encoded key of the entity, generated, globally unique | [optional] 
**user_transaction_time** | **String** | The formatted time at which the user made this card transaction. | [optional] 
**currency_code** | **String** | The ISO currency code in which the card reversal transaction is posted. The amounts are stored in the base currency, but the transaction can be created with a foreign currency. | [optional] 
**card_token** | **String** | The reference token of the card. | [optional] 


