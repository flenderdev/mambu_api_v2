# MambuApiV2::GetAuthorizationHold

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Float** | The amount of money to be held as a result of the authorization hold request. | 
**advice** | **BOOLEAN** | Whether the given request should be accepted without balance validations. | 
**card_acceptor** | [**CardAcceptor**](CardAcceptor.md) | The card acceptor details. | [optional] 
**credit_debit_indicator** | **String** | Indicates whether the authorization hold amount is credited or debited.If not provided, the default values is DBIT. | [optional] 
**card_token** | **String** | The reference token of the card. | [optional] 
**external_reference_id** | **String** | The external reference ID to be used to reference the account hold in subsequent requests. | 
**original_amount** | **Float** | The original amount of money to be held as a result of the authorization hold request. | [optional] 
**exchange_rate** | **Float** | The exchange rate for the original currency. | [optional] 
**encoded_key** | **String** | The internal ID of the authorization hold, auto generated, unique. | [optional] 
**user_transaction_time** | **String** | The formatted time at which the user made this authorization hold. | [optional] 
**original_currency** | **String** | The original currency in which the hold was created. | [optional] 
**currency_code** | **String** | The ISO currency code in which the hold was created. The amounts are stored in the base currency, but the user could have enter it in a foreign currency. | [optional] 
**status** | **String** | The authorization hold status. | [optional] 


