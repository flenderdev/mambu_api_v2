# MambuApiV2::LoanAccountSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**installments** | [**Array&lt;Installment&gt;**](Installment.md) | The loan account schedule installments&#39; list. | [optional] 


