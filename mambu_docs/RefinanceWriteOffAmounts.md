# MambuApiV2::RefinanceWriteOffAmounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interest** | **Float** | Interest write-off amount | [optional] 
**penalty** | **Float** | Penalty write-off amount | [optional] 
**fee** | **Float** | Fee write-off amount | [optional] 


