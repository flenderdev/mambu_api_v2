# MambuApiV2::PenaltySettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loan_penalty_calculation_method** | **String** | The last penalty calculation method, represents on what amount are the penalties calculated. | [optional] 
**penalty_rate** | **Float** | The penalty rate, represents the rate (in percent) which is charged as a penalty. | [optional] 


