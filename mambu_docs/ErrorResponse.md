# MambuApiV2::ErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | [**Array&lt;RestError&gt;**](RestError.md) |  | [optional] 


