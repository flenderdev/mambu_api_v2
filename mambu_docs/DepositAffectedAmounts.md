# MambuApiV2::DepositAffectedAmounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fees_amount** | **Float** | Amount of fees involved in a transaction that affects an account with positive balance | [optional] 
**overdraft_interest_amount** | **Float** | Interest amount involved in a transaction that affects an overdraft | [optional] 
**overdraft_fees_amount** | **Float** | Fees amount involved in a transaction that affects an overdraft | [optional] 
**fraction_amount** | **Float** | In the case of an LOAN_FRACTION_BOUGHT this represent the fraction amount which was bought from another investor | [optional] 
**technical_overdraft_amount** | **Float** | The amount of money that was added/subtracted from the account by this transaction as technical overdraft | [optional] 
**overdraft_amount** | **Float** | The amount of money that was added/subtracted from the account by this transaction as overdraft | [optional] 
**interest_amount** | **Float** | Amount of interest involved in a transaction that affects an account with positive balance | [optional] 
**technical_overdraft_interest_amount** | **Float** | The amount of money that was added/subtracted from the account by this transaction as technical overdraft interest | [optional] 
**funds_amount** | **Float** | Balance change amount involved in a transaction that affects an account with positive balance | [optional] 


