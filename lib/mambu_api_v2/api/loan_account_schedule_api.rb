=begin
#loans

#No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

OpenAPI spec version: v2

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.18

=end

require 'uri'

module MambuApiV2
  class LoanAccountScheduleApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Allows retrieval of a loan account schedule for non existing loan account
    # 
    # @param body Loan account parameters for a schedule to be previewed
    # @param [Hash] opts the optional parameters
    # @return [LoanAccountSchedule]
    def get_preview_loan_account_schedule(body, opts = {})
      data, _status_code, _headers = get_preview_loan_account_schedule_with_http_info(body, opts)
      data
    end

    # Allows retrieval of a loan account schedule for non existing loan account
    # 
    # @param body Loan account parameters for a schedule to be previewed
    # @param [Hash] opts the optional parameters
    # @return [Array<(LoanAccountSchedule, Fixnum, Hash)>] LoanAccountSchedule data, response status code and response headers
    def get_preview_loan_account_schedule_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: LoanAccountScheduleApi.get_preview_loan_account_schedule ...'
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling LoanAccountScheduleApi.get_preview_loan_account_schedule"
      end
      # resource path
      local_var_path = '/loans:previewSchedule'

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/vnd.mambu.v2+json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basic']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'LoanAccountSchedule')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LoanAccountScheduleApi#get_preview_loan_account_schedule\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Allows retrieval of a loan account schedule by provided loan account id or encodedKey.
    # 
    # @param loan_account_id The id or encoded key of the loan account
    # @param [Hash] opts the optional parameters
    # @return [LoanAccountSchedule]
    def get_schedule_for_loan_account(loan_account_id, opts = {})
      data, _status_code, _headers = get_schedule_for_loan_account_with_http_info(loan_account_id, opts)
      data
    end

    # Allows retrieval of a loan account schedule by provided loan account id or encodedKey.
    # 
    # @param loan_account_id The id or encoded key of the loan account
    # @param [Hash] opts the optional parameters
    # @return [Array<(LoanAccountSchedule, Fixnum, Hash)>] LoanAccountSchedule data, response status code and response headers
    def get_schedule_for_loan_account_with_http_info(loan_account_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: LoanAccountScheduleApi.get_schedule_for_loan_account ...'
      end
      # verify the required parameter 'loan_account_id' is set
      if @api_client.config.client_side_validation && loan_account_id.nil?
        fail ArgumentError, "Missing the required parameter 'loan_account_id' when calling LoanAccountScheduleApi.get_schedule_for_loan_account"
      end
      # resource path
      local_var_path = '/loans/{loanAccountId}/schedule'.sub('{' + 'loanAccountId' + '}', loan_account_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/vnd.mambu.v2+json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basic']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'LoanAccountSchedule')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LoanAccountScheduleApi#get_schedule_for_loan_account\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
