module MambuApiV2
  class BaseService
    def self.call(*args)
      obj = new(*args)
      obj.call
    end
  end
end