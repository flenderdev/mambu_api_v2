require 'json'

module MambuApiV2
  module LoanAccounts
    class SearchWithHttpInfoService < BaseService
      attr_reader :payload, :http_option

      def initialize(payload, http_option)
        @payload = payload,
        @http_option = http_option
      end

      def call
        api_instance.search_with_http_info(body, http_option).first
      end

      private

      def api_instance
        @api_instance ||= ::MambuApiV2::LoanAccountsSearchApi.new
      end

      def body
        @body ||= ::MambuApiV2::LoanAccountSearchCriteria.new(payload.first)
      end

    end
  end
end
