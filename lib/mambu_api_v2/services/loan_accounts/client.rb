# frozen_string_literal: true

module MambuApiV2
    module LoanAccounts
        class Client
            def self.search_with_http_info(payload, http_options)
                ::MambuApiV2::LoanAccounts::SearchWithHttpInfoService.call(payload, http_options)
            end
        end
    end
end
