require 'json'

module MambuApiV2
  module Installments
    class GetInstallmentService < BaseService
      attr_reader :due_from, :due_to

      def initialize(due_from, due_to)
        @due_from = due_from
        @due_to = due_to
      end

      def call
        offset = 0
        limit = 1000

        result = api_instance.get_all(due_from, due_to, payload(offset, limit))
        offset += limit

        while result.size >= 1000
          current_result = api_instance.get_all(due_from, due_to, payload(offset, limit))
          result += current_result
          break if current_result.empty? || current_result.size < 1000

          offset += limit
        end

        result
      end

      private

      def api_instance
        @api_instance ||= ::MambuApiV2::InstallmentsApi.new
      end

      def payload(offset, limit)
        {
          offset: offset,
          limit: limit
        }
      end
    end
  end
end
