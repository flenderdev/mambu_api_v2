# frozen_string_literal: true

module MambuApiV2
    module Installments
        class Client
            def self.get_installments(due_from, due_to)
                ::MambuApiV2::Installments::GetInstallmentService.call(due_from, due_to)
            end
        end
    end
end
