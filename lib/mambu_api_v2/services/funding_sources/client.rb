# frozen_string_literal: true

module MambuApiV2
  module FundingSources
    class Client
      def self.sell(funding_source_id, purchases)
        ::MambuApiV2::FundingSources::SellService.call(funding_source_id, purchases)
      end
    end
  end
end
