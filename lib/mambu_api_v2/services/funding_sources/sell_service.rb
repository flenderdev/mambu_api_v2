require 'json'

module MambuApiV2
  module FundingSources
    class SellService < BaseService
      attr_reader :funding_source_id, :purchases

      def initialize(funding_source_id, purchases)
        @funding_source_id = funding_source_id
        @purchases = ::MambuApiV2::ArrayNormalizer.normalize(purchases)
      end

      def call
        api_instance.sell(funding_source_id, body)
      end

      private

      def api_instance
        @api_instance ||= ::MambuApiV2::FundingSourcesApi.new
      end

      def body
        @body ||= ::MambuApiV2::SellFundingSourceAction.new(purchases: purchases_to_body)
      end

      def purchases_to_body
        purchases.map do |purchase|
          check_purchase_type(purchase)
          purchase = purchase_to_model(purchase)
          raise MambuApiV2::ServiceError.new(purchase.list_invalid_properties.join) unless purchase.valid?

          purchase.to_hash
        end
      end

      def purchase_to_model(purchase)
        return purchase if purchase.is_a?(::MambuApiV2::FundingSourcePurchase)

        # Convert key to JSON style from ruby-style
        purchase_json_style = purchase.each_with_object({}) do |(k, v), h|
          next if k.nil?
          h[::MambuApiV2::FundingSourcePurchase.attribute_map[k.to_sym]] = v
        end
        ::MambuApiV2::FundingSourcePurchase.new(purchase.merge(purchase_json_style).delete_if{|k, _v| k.nil?})
      end

      def check_purchase_type(purchase)
        unless purchase.is_a?(Hash) || purchase.is_a?(::MambuApiV2::FundingSourcePurchase)
          message = "Invalid argument type. Expected Hash or MambuApiV2::FundingSourcePurchase, got #{purchase.class}"
          raise MambuApiV2::ServiceError.new(message)
        end
      end
    end
  end
end

