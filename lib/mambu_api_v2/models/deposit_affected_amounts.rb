=begin
#fundingsources

#No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

OpenAPI spec version: v2

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.17

=end

require 'date'

module MambuApiV2
  # The amounts affected after completing the deposit transaction
  class DepositAffectedAmounts
    # Amount of fees involved in a transaction that affects an account with positive balance
    attr_accessor :fees_amount

    # Interest amount involved in a transaction that affects an overdraft
    attr_accessor :overdraft_interest_amount

    # Fees amount involved in a transaction that affects an overdraft
    attr_accessor :overdraft_fees_amount

    # In the case of an LOAN_FRACTION_BOUGHT this represent the fraction amount which was bought from another investor
    attr_accessor :fraction_amount

    # The amount of money that was added/subtracted from the account by this transaction as technical overdraft
    attr_accessor :technical_overdraft_amount

    # The amount of money that was added/subtracted from the account by this transaction as overdraft
    attr_accessor :overdraft_amount

    # Amount of interest involved in a transaction that affects an account with positive balance
    attr_accessor :interest_amount

    # The amount of money that was added/subtracted from the account by this transaction as technical overdraft interest
    attr_accessor :technical_overdraft_interest_amount

    # Balance change amount involved in a transaction that affects an account with positive balance
    attr_accessor :funds_amount

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'fees_amount' => :'feesAmount',
        :'overdraft_interest_amount' => :'overdraftInterestAmount',
        :'overdraft_fees_amount' => :'overdraftFeesAmount',
        :'fraction_amount' => :'fractionAmount',
        :'technical_overdraft_amount' => :'technicalOverdraftAmount',
        :'overdraft_amount' => :'overdraftAmount',
        :'interest_amount' => :'interestAmount',
        :'technical_overdraft_interest_amount' => :'technicalOverdraftInterestAmount',
        :'funds_amount' => :'fundsAmount'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'fees_amount' => :'Float',
        :'overdraft_interest_amount' => :'Float',
        :'overdraft_fees_amount' => :'Float',
        :'fraction_amount' => :'Float',
        :'technical_overdraft_amount' => :'Float',
        :'overdraft_amount' => :'Float',
        :'interest_amount' => :'Float',
        :'technical_overdraft_interest_amount' => :'Float',
        :'funds_amount' => :'Float'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h| h[k.to_sym] = v }

      if attributes.has_key?(:'feesAmount')
        self.fees_amount = attributes[:'feesAmount']
      end

      if attributes.has_key?(:'overdraftInterestAmount')
        self.overdraft_interest_amount = attributes[:'overdraftInterestAmount']
      end

      if attributes.has_key?(:'overdraftFeesAmount')
        self.overdraft_fees_amount = attributes[:'overdraftFeesAmount']
      end

      if attributes.has_key?(:'fractionAmount')
        self.fraction_amount = attributes[:'fractionAmount']
      end

      if attributes.has_key?(:'technicalOverdraftAmount')
        self.technical_overdraft_amount = attributes[:'technicalOverdraftAmount']
      end

      if attributes.has_key?(:'overdraftAmount')
        self.overdraft_amount = attributes[:'overdraftAmount']
      end

      if attributes.has_key?(:'interestAmount')
        self.interest_amount = attributes[:'interestAmount']
      end

      if attributes.has_key?(:'technicalOverdraftInterestAmount')
        self.technical_overdraft_interest_amount = attributes[:'technicalOverdraftInterestAmount']
      end

      if attributes.has_key?(:'fundsAmount')
        self.funds_amount = attributes[:'fundsAmount']
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          fees_amount == o.fees_amount &&
          overdraft_interest_amount == o.overdraft_interest_amount &&
          overdraft_fees_amount == o.overdraft_fees_amount &&
          fraction_amount == o.fraction_amount &&
          technical_overdraft_amount == o.technical_overdraft_amount &&
          overdraft_amount == o.overdraft_amount &&
          interest_amount == o.interest_amount &&
          technical_overdraft_interest_amount == o.technical_overdraft_interest_amount &&
          funds_amount == o.funds_amount
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Fixnum] Hash code
    def hash
      [fees_amount, overdraft_interest_amount, overdraft_fees_amount, fraction_amount, technical_overdraft_amount, overdraft_amount, interest_amount, technical_overdraft_interest_amount, funds_amount].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.swagger_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :BOOLEAN
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        temp_model = MambuApiV2.const_get(type).new
        temp_model.build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        next if value.nil?
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end

  end
end
