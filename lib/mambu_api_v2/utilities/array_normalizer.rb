module MambuApiV2
  class ArrayNormalizer
    def self.normalize(array)
      return nil if array.nil?
      return array if array.is_a?(Array)

      [array]
    end
  end
end